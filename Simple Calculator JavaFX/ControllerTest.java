package sample;

import javafx.scene.Parent;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ControllerTest {

    @Test
    public void shouldAddPositiveNumbersPass() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 1;
        String sign = "+";
        assertEquals(4, 4);
    }

    @Test
    public void shouldAddNegativeNumbersPass() {
        Controller controller = new Controller();
        double number1 = -1;
        double number2 = -1;
        String sign = "+";
        assertEquals(-2, -2);
    }

    @Test
    public void shouldAddZeroPass() {
        Controller controller = new Controller();
        double number1 = 0;
        double number2 = 4;
        String sign = "+";
        assertEquals(4, 4);
    }

    @Test
    public void shouldAddFailed() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 1;
        String sign = "+";
        assertNotEquals(2, 4);
    }

    @Test
    public void shouldSubtractPositiveNumbersPass() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 1;
        String sign = "-";
        assertEquals(2, 2);
    }

    @Test
    public void shouldSubtractNegativeNumbersPass() {
        Controller controller = new Controller();
        double number1 = -1;
        double number2 = -1;
        String sign = "-";
        assertEquals(0, 0);
    }

    @Test
    public void shouldSubtractZeroPass() {
        Controller controller = new Controller();
        double number1 = 0;
        double number2 = 4;
        String sign = "-";
        assertEquals(-4, -4);
    }

    @Test
    public void shouldSubtractFailed() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 1;
        String sign = "-";
        assertNotEquals(9, 2);
    }

    @Test
    public void shouldMultiplyPositiveNumbersPass() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 1;
        String sign = "*";
        assertEquals(3, 3);
    }

    @Test
    public void shouldMultiplyNegativeNumbersPass() {
        Controller controller = new Controller();
        double number1 = 1;
        double number2 = -1;
        String sign = "*";
        assertEquals(-1, -1);
    }

    @Test
    public void shouldMultiplyZeroPass() {
        Controller controller = new Controller();
        double number1 = 0;
        double number2 = 4;
        String sign = "*";
        assertEquals(0, 0);
    }

    @Test
    public void shouldMultiplyFailed() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 1;
        String sign = "*";
        assertNotEquals(2, 3);
    }

    @Test
    public void shouldDividePositiveNumbersPass() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 2;
        String sign = "/";
        assertEquals(1.5, 1.5);
    }

    @Test
    public void shouldDivideNegativeNumbersPass() {
        Controller controller = new Controller();
        double number1 = -1;
        double number2 = 4;
        String sign = "/";
        assertEquals(0.25, 0.25);
    }

    @Test
    public void shouldDivideZeroPass() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 0;
        String sign = "/";
        assertEquals("Don't divide by zero", "Don't divide by zero");
    }

    @Test
    public void shouldDivideFailed() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 1;
        String sign = "/";
        assertNotEquals(9, 3);
    }

    @Test
    public void shouldNotCountPass() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 1;
        String sign = "p";
        assertEquals("You should choose only \n'+', '-', '*' or '/'",
                "You should choose only \n'+', '-', '*' or '/'");
    }

    @Test
    public void shouldNotCountFailed() {
        Controller controller = new Controller();
        double number1 = 3;
        double number2 = 1;
        String sign = "p";
        assertNotEquals(3, "You should choose only \n'+', '-', '*' or '/'");
    }
}