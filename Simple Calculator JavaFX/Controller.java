package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;

public class Controller {

    @FXML
    TextField firstNumber;

    @FXML
    TextField secondNumber;

    @FXML
    TextField arithmeticSign;

    @FXML
    Label resultField;

    @FXML
    private void handleButtonAction() {
        double number1 = Double.parseDouble(firstNumber.getText());
        double number2 = Double.parseDouble(secondNumber.getText());
        String sign = arithmeticSign.getText();
        resultField.setText(calculate(number1, number2, sign));
    }

    public static String calculate( double number1, double number2, String sign) {

        String result;
        switch (sign) {
            case ("+"):
                result = Double.toString(number1 + number2);
                return result;
            case ("-"):
                result = Double.toString(number1 - number2);
                return result;
            case ("*"):
                result = Double.toString(number1 * number2);
                return result;
            case ("/"):
                if (number2 == 0){
                    result = "Don't divide by zero";
                    return result;}
            {
                result = Double.toString(number1 / number2);
                return result;}
            default:
                result = "You should choose only \n'+', '-', '*' or '/'";
                return result;
        }
    }
}